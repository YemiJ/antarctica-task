resource "aws_iam_role" "lambda_role" {
  name               = "${local.prefix}-lambda-role"
  assume_role_policy = <<EOF
{
 "Version": "2012-10-17",
 "Statement": [
   {
     "Action": "sts:AssumeRole",
     "Principal": {
       "Service": ["lambda.amazonaws.com",
                   "redshift.amazonaws.com",
                   "iam.amazonaws.com"
      ]
     },
     "Effect": "Allow",
     "Sid": ""
   }
 ]
}
EOF
}

resource "aws_iam_policy" "iam_policy_for_lambda" {

  name        = "aws_iam_policy_for_terraform_aws_lambda_role"
  path        = "/"
  description = "AWS IAM Policy for managing aws lambda role"
  policy      = <<EOF
{
 "Version": "2012-10-17",
 "Statement": [
  {
     "Action": [
       "s3:ListBucket",
       "s3:GetObject",
       "s3:CopyObject",
       "s3:HeadObject",
       "s3:PutObject"
     ],
     "Effect": "Allow",
     "Resource": "*"
  },
 {
     "Action": [
       "logs:CreateLogGroup",
       "logs:CreateLogStream",
       "logs:PutLogEvents"
     ],
     "Resource": "arn:aws:logs:*:*:*",
     "Effect": "Allow"
  },
 {
		"Effect": "Allow",
		"Action": "redshift:*",
		"Resource": "*"
}
 ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "attach_iam_policy_to_iam_role" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = aws_iam_policy.iam_policy_for_lambda.arn
}
resource "aws_iam_role_policy" "s3_full_access_policy" {
  name   = "redshift_s3_policy"
  role   = aws_iam_role.redshift_role.id
  policy = <<EOF
{
	"Version": "2012-10-17",
	"Statement": [{
		"Effect": "Allow",
		"Action": "s3:*",
		"Resource": "*"
	}
    ]
}
  EOF
}

resource "aws_iam_role" "redshift_role" {
  name               = "redshift-role"
  assume_role_policy = <<EOF
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": "sts:AssumeRole",
        "Principal": {
        "Service": ["s3.amazonaws.com",
                    "redshift.amazonaws.com",
                    "iam.amazonaws.com",
                    "redshift-serverless.amazonaws.com"
            ]
          },
        "Effect": "Allow",
        "Sid": ""
      }
  ]
  }
  EOF
  tags = {
    tag-key = "redshift-role"
  }
}
