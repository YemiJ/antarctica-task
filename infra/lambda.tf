resource "aws_lambda_function" "lambda1raw_to_cleans3" {
  image_uri     = "650774927638.dkr.ecr.us-east-1.amazonaws.com/${var.lambda1-function-name}:latest"
  package_type  = "Image"
  function_name = "${local.prefix}-lambda1"
  role          = aws_iam_role.lambda_role.arn
  timeout       = 300

  environment {
    variables = {
      clean_s3_bucket_name           = "${local.prefix}-${var.clean-s3-bucket-name}"
      fund_details_cleaned_file_name = var.fund_details_cleaned_file_name
      fund_prices_cleaned_file_name  = var.fund_prices_cleaned_file_name
    }
  }
  memory_size = 10240

  ephemeral_storage {
    size = 10240
  }
}

resource "aws_lambda_permission" "lambda1_allow_bucket_notify" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda1raw_to_cleans3.arn
  principal     = "s3.amazonaws.com"
  source_arn    = "arn:aws:s3:::${aws_s3_bucket.raw-s3-bucket.bucket}"
}

resource "aws_s3_bucket_notification" "aws-lambda1-trigger" {
  bucket = aws_s3_bucket.raw-s3-bucket.bucket
  lambda_function {
    lambda_function_arn = aws_lambda_function.lambda1raw_to_cleans3.arn
    events              = ["s3:ObjectCreated:*"]
  }

  depends_on = [aws_lambda_permission.lambda1_allow_bucket_notify]
}

resource "aws_lambda_function" "lambda2_cleans3_to_transformeds3_and_redshift" {
  image_uri     = "650774927638.dkr.ecr.us-east-1.amazonaws.com/${var.lambda2-function-name}:latest"
  package_type  = "Image"
  function_name = "${local.prefix}-lambda2"
  role          = aws_iam_role.lambda_role.arn
  timeout       = 300

  environment {
    variables = {
      clean_s3_bucket_name           = "${local.prefix}-${var.clean-s3-bucket-name}"
      fund_details_cleaned_file_name = var.fund_details_cleaned_file_name
      fund_prices_cleaned_file_name  = var.fund_prices_cleaned_file_name
      db_host                        = aws_redshift_cluster.redshift-cluster.endpoint
      db_port                        = var.db_port
      db_name                        = aws_redshift_cluster.redshift-cluster.database_name
      db_user                        = aws_redshift_cluster.redshift-cluster.master_username
      db_password                    = aws_redshift_cluster.redshift-cluster.master_password
      db_role                        = aws_iam_role.redshift_role.arn
      transformed_s3_bucket_name     = "${local.prefix}-${var.transformed-s3-bucket-name}"
      ytd_details_merged_file_name   = var.ytd_details_merged_file_name
    }
  }

  memory_size = 10240

  ephemeral_storage {
    size = 10240
  }

  depends_on = [aws_redshift_cluster.redshift-cluster]
}

resource "aws_lambda_permission" "lambda2_allow_bucket_notify" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda2_cleans3_to_transformeds3_and_redshift.arn
  principal     = "s3.amazonaws.com"
  source_arn    = "arn:aws:s3:::${aws_s3_bucket.clean-s3-bucket.bucket}"
}

resource "aws_s3_bucket_notification" "aws-lambda2-trigger" {
  bucket = aws_s3_bucket.clean-s3-bucket.bucket
  lambda_function {
    lambda_function_arn = aws_lambda_function.lambda2_cleans3_to_transformeds3_and_redshift.arn
    events              = ["s3:ObjectCreated:*"]
  }

  depends_on = [aws_lambda_permission.lambda2_allow_bucket_notify]
}