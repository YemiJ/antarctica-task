resource "aws_vpc" "redshift-vpc" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"
  tags = {
    Name = "redshift-vpc"
  }
}

resource "aws_internet_gateway" "redshift-vpc-gateway" {
  vpc_id = aws_vpc.redshift-vpc.id
  depends_on = [
    aws_vpc.redshift-vpc
  ]
}

resource "aws_default_security_group" "redshift-security-group" {
  vpc_id = aws_vpc.redshift-vpc.id
  ingress {
    from_port   = 5439
    protocol    = "tcp"
    to_port     = 5439
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "redshift-security-group"
  }
  depends_on = [aws_vpc.redshift-vpc]
}

resource "aws_subnet" "redshift-subnet-1" {
  vpc_id            = aws_vpc.redshift-vpc.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-east-1a"
  tags = {
    Name = "redshift-subnet-1"
  }
  depends_on = [
    aws_vpc.redshift-vpc
  ]
}

resource "aws_subnet" "redshift-subnet-2" {
  vpc_id            = aws_vpc.redshift-vpc.id
  cidr_block        = "10.0.2.0/24"
  availability_zone = "us-east-1b"
  tags = {
    Name = "redshift-subnet-2"
  }
  depends_on = [
    aws_vpc.redshift-vpc
  ]
}

resource "aws_redshift_subnet_group" "redshift-subnet-group" {
  name       = "redshift-subnet-group"
  subnet_ids = [aws_subnet.redshift-subnet-1.id, aws_subnet.redshift-subnet-2.id]
  tags = {
    Name = "redshift-subnet-group"
  }
}

resource "aws_route_table" "redshift-vpc-route-table" {
  vpc_id = aws_vpc.redshift-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.redshift-vpc-gateway.id
  }

  route {
    ipv6_cidr_block = "::/0"
    gateway_id      = aws_internet_gateway.redshift-vpc-gateway.id
  }
}

resource "aws_route_table_association" "redshift-vpc-route-table-assoc-1" {
  route_table_id = aws_route_table.redshift-vpc-route-table.id
  subnet_id      = aws_subnet.redshift-subnet-1.id
}

resource "aws_route_table_association" "redshift-vpc-route-table-assoc-2" {
  route_table_id = aws_route_table.redshift-vpc-route-table.id
  subnet_id      = aws_subnet.redshift-subnet-2.id
}