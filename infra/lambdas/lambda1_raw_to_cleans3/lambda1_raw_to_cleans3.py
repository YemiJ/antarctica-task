import os
import boto3
import logging
import pandas as pd

# Configure logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)

s3_client = boto3.client('s3')

object_columns = ['fund_symbol', 'quote_type', 'region', 'fund_short_name',
                  'fund_long_name', 'currency', 'fund_category', 'fund_family',
                  'exchange_code', 'exchange_name', 'exchange_timezone',
                  'investment_strategy', 'inception_date', 'investment_type',
                  'size_type', 'top10_holdings', 'returns_as_of_date', 'fund_symbol',
                  'price_date']

int_64_columns = ['avg_vol_3month']

def lambda_handler(event, context):
    print('event collected is {}'.format(event))
    try:
        for i in event['Records']:
            bucket_name = i["s3"]["bucket"]["name"]
            s3_file_name = i["s3"]["object"]["key"]
            response = s3_client.get_object(Bucket=bucket_name, Key=s3_file_name)

            if s3_file_name == "Fund_Details.csv":
                fund_details_df = pd.read_csv(response['Body'], sep=',')
                validate_types(fund_details_df, object_columns, int_64_columns)
                fund_details_df_denulled = handle_missing_values(fund_details_df, object_columns)
                fund_details_df_deduped = handle_duplicate_values(fund_details_df_denulled)

                upload_data_frame_to_s3(fund_details_df_deduped, os.environ['clean_s3_bucket_name'],
                                        os.environ['fund_details_cleaned_file_name'])

            elif s3_file_name == "Fund_Prices.csv":
                fund_prices_df = pd.read_csv(response['Body'], sep=',')
                validate_types(fund_prices_df, object_columns, int_64_columns)
                fund_prices_df_denulled = handle_missing_values(fund_prices_df, object_columns)
                fund_prices_df_deduped = handle_duplicate_values(fund_prices_df_denulled)

                upload_data_frame_to_s3(fund_prices_df_deduped, os.environ['clean_s3_bucket_name'],
                                        os.environ['fund_prices_cleaned_file_name'])

    except Exception as err:
        logger.error(f"An error occurred: {str(err)}")

def validate_types(df, object_columns, int_64_columns):
    """
    Validates the data types and formats in a fund_prices and fund_details DataFrame.

    Parameters:
        df (pd.DataFrame): The DataFrame to be validated.
        object_columns (list): List of columns that should be of object type
        int_64_columns (list): List of columns that should be of int64 type

    Raises:
        ValueError: If any validation criteria are not met.
    """
    has_valid_types = True
    for col in df.columns:
        if col in object_columns and df[col].dtype != 'object':
            logger.error("An error occurred: %s",
                         f"Column '{col}' should have object values.")
            raise ValueError('')


        if col in int_64_columns and df[col].dtype != 'int64':
            logger.error("An error occurred: %s",
                         f"Column '{col}' should have integer 64 values.")
            raise

        if col not in object_columns and col not in int_64_columns and df[col].dtype != 'float64':
            logger.error("An error occurred: %s",
                         f"Column '{col}' should have float64 values.")
            raise

        if df[col].dtype not in ['float64', 'object', 'int64']:
            logger.error("An error occurred: %s",
                         "Unsupported data format")
            raise

    return True

def handle_missing_values(df, object_columns):
    """
        Handles missing values in the fund prices/fund details df DataFrame using the specified method.

        Parameters:
            df (pd.DataFrame): The fund prices/fund details DataFrame to handle missing values in.
            object_columns (list): List of columns that should be of object type
            int_64_columns (list): List of columns that should be of int64 type

        Returns:
            pd.DataFrame: Fund prices DataFrame with missing values handled.
        """
    cols = df.columns

    for col in cols:

        if col not in object_columns:
            # fill with mean value for floats and integers
            mean_value = df[col].mean()
            df.loc[:,[col]] = df.loc[:,[col]].fillna(value=mean_value)

        if col == 'price_date':
            # remove fund prices rows with missing dates entirely
            df = df[df['price_date'].notna()]

    return df
def handle_duplicate_values(df):
    """
    Handles missing values in the fund prices/fund details df DataFrame using the specified method.

    Parameters:
        df (pd.DataFrame): The fund prices/fund details DataFrame to handle dup rows in.

    Returns:
        pd.DataFrame: Fund prices/fund details DataFrame with duplicate rows handled.
    """
    if 'price_date' in df.columns:
        # check if its the fund prices data
        return df.drop_duplicates(['fund_symbol', 'price_date'], keep='first')
    elif 'fund_short_name' in df.columns:
        # check if its the fund details data
        return df.drop_duplicates(['fund_symbol'], keep='first')
    else:
        return df.drop_duplicates(df.columns, keep='first')


def upload_data_frame_to_s3(df, bucket_name, file_name):
    """
    Uploads a pandas DataFrame to an S3 bucket.

    Parameters:
        df (pd.DataFrame): The DataFrame to upload.
        bucket_name (str): The name of the S3 bucket.
        file_name (str): The name of the file to be uploaded.
    """
    try:
        csv_data = df.to_csv(index=False)
        file_name = f"{file_name}.csv"

        s3_client.put_object(Body=csv_data, Bucket=bucket_name, Key=file_name)

        logger.info(f"DataFrame copied to S3 bucket '{bucket_name}' as '{file_name}'")
    except Exception as e:
        logger.error("An error occurred: %s", str(e))
        raise