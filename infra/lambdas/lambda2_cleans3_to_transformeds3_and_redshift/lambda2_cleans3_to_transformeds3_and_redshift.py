import os
import io
import boto3
import json
import logging
import psycopg2
import pandas as pd
import pyarrow as pa
import pyarrow.parquet as pq


# Configure logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)

# Redshift connection details
db_host = os.environ['db_host']
db_port = os.environ['db_port']
db_name = os.environ['db_name']
db_user = os.environ['db_user']
db_password = os.environ['db_password']
db_role= os.environ['db_role']
db_table_name = 'fundDetailsTransformed'

s3_client = boto3.client('s3')

def lambda_handler(event, context):
    print('event collected is {}'.format(event))
    try:
        clean_s3_bucket_name = os.environ['clean_s3_bucket_name']
        transformed_s3_bucket_name = os.environ['transformed_s3_bucket_name']
        fund_prices_cleaned_file_name = os.environ['fund_prices_cleaned_file_name']
        fund_details_cleaned_file_name = os.environ['fund_details_cleaned_file_name']
        ytd_details_merged_file_name = os.environ['ytd_details_merged_file_name']

        # Check if both files exist on trigger
        fund_prices_file = s3_client.head_object(Bucket=clean_s3_bucket_name,
                                                 Key=fund_prices_cleaned_file_name)
        fund_details_file = s3_client.head_object(Bucket=clean_s3_bucket_name,
                                                   Key=fund_details_cleaned_file_name)

        fund_prices_file_content = s3_client.get_object(Bucket=clean_s3_bucket_name,
                                                        Key=fund_prices_cleaned_file_name)
        fund_details_file_content = s3_client.get_object(Bucket=clean_s3_bucket_name,
                                                        Key=fund_details_cleaned_file_name)

        fund_details_df = pd.read_csv(fund_details_file_content['Body'], sep=',')
        fund_prices_df = pd.read_csv(fund_prices_file_content['Body'], sep=',')

        fund_ytd_df = fund_prices_df.groupby('fund_symbol').apply(calculate_ytd).reset_index()[['fund_symbol', 'YTD']]
        fund_ytd_details_df = pd.merge(fund_ytd_df, fund_details_df, on='fund_symbol', how='left')

        upload_data_frame_to_s3_optimised(fund_ytd_details_df, transformed_s3_bucket_name, ytd_details_merged_file_name)
        upload_data_frame_to_redshift(fund_ytd_details_df, db_name, db_host, db_port, db_user, db_password, db_role, transformed_s3_bucket_name, ytd_details_merged_file_name)

    except Exception as err:
        logger.error(f"An error occurred: {str(err)}")


def calculate_ytd(group):
    """
    Calculate YTD for each fund group
    Parameters:
        df (pd.DataFrame): The DataFrame for fund group.

    Raises:
        (pd.DataFrame): Dataframe of YTD for given group
    """
    try:
        starting_price = list(group[(group['price_date'] == '2020-11-30')]['open'].values)[0]
        ending_price = list(group[(group['price_date'] == '2021-11-30')]['close'].values)[0]
        YTD = (ending_price - starting_price) / starting_price
    except:
        YTD = None
        # could send invalid YTD calculations to a different s3 bucket for processing

    return pd.DataFrame({'YTD': [YTD]})

def upload_data_frame_to_s3_optimised(df, bucket_name, file_name):
    """
    Uploads a pandas DataFrame to an S3 bucket as parquet file.

    Parameters:
        df (pd.DataFrame): The DataFrame to upload.
        bucket_name (str): The name of the S3 bucket.
        file_name (str): The name of the file to be uploaded.
    """
    try:
        # convert dtaframe to Parquet for optimised queries
        table = pa.Table.from_pandas(df)
        parquet_stream = io.BytesIO()
        pq.write_table(table, parquet_stream)

        s3_key = f"{file_name}.parquet"
        s3_client.put_object(Bucket=bucket_name, Key=s3_key, Body=parquet_stream.getvalue())

        # store some metadata
        metadata = {
            'description': 'Parquet dataset optimised for business analysis',
            'columns': list(df.columns),
            'row_count': len(df)
        }

        metadata_key = f"{file_name}-metadata.json"
        s3_client.put_object(Bucket=bucket_name, Key=metadata_key, Body=json.dumps(metadata))

        # store as csv for moving to redshift
        csv_data = df.to_csv(index=False)
        file_name = f"{file_name}.csv"

        s3_client.put_object(Body=csv_data, Bucket=bucket_name, Key=file_name)

        logger.info(f"DataFrame copied to S3 bucket '{bucket_name}' as '{file_name}'")
    except Exception as e:
        logger.error("An error occurred: %s", str(e))
        raise

def upload_data_frame_to_redshift(df, db_name, db_host, db_port,
                                  db_user, db_password, db_role,
                                  bucket_name, file_name):
    """
    Saves a pandas DataFrame to a Redshift table.

    Parameters:
        df (pd.DataFrame): The DataFrame to be saved.
        table_name (str): The name of the target Redshift table.
        db_username (str): Redshift username.
        db_password (str): Redshift password.
        db_host (str): Redshift host URL.
        db_port (str): Redshift port.
        db_name (str): Redshift database name.
    """
    csv_buffer = io.StringIO()
    df.to_csv(csv_buffer, index=False, header=False, sep='\t')
    csv_buffer.seek(0)

    conn = psycopg2.connect(
        dbname=db_name,
        host=db_host.split(':')[0],
        port=db_port,
        user=db_user,
        password=db_password
    )

    cursor = conn.cursor()

    create_redshift_tables(df, cursor, conn, db_table_name)

    from_path = f"s3://{bucket_name}/{file_name}.csv"

    cp_tables = "copy " + db_table_name + " from '{}' iam_role '{}' csv ignoreheader 1;".format(from_path, db_role)

    cursor.execute(cp_tables)
    conn.commit()


def create_redshift_tables(df, cur, conn, table_name):
    """
    Creates table in Amazon Redshift based on DataFrame columns.

    Parameters:
        df (pd.DataFrame): The DataFrame whose columns will be used for table creation.
        table_name (str): The name of the target table.
        cur: redsdhift cursor
        conn: redshift connection
    """
    columns = []
    for col_name, col_type in df.dtypes.items():
        if pd.api.types.is_integer_dtype(col_type):
            data_type = 'INTEGER'
        elif pd.api.types.is_float_dtype(col_type):
            data_type = 'FLOAT'
        elif pd.api.types.is_string_dtype(col_type):
            data_type = 'VARCHAR(1000)'
        else:
            data_type = 'VARCHAR(255)'  # Default to VARCHAR if data type is not recognized

        columns.append(f"{col_name} {data_type}")

    columns_sql = ",\n".join(columns)
    cr_tables_sql = f"CREATE TABLE IF NOT EXISTS {table_name} (\n{columns_sql}\n);"

    cur.execute(cr_tables_sql)
    conn.commit()