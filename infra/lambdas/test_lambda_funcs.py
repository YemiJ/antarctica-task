####### Example tests
import unittest
import pandas as pd
from lambda1_raw_to_cleans3.lambda1_raw_to_cleans3 import validate_types, handle_missing_values, handle_duplicate_values
from lambda2_cleans3_to_transformeds3_and_redshift.lambda2_cleans3_to_transformeds3_and_redshift import calculate_ytd

class TestLambda(unittest.TestCase):
    def test_validate_types(self):
        '''Test functions returns error given invalid data types'''
        data = {'col1': [1, 2, 3],
                'col2': ['a', 'b', 'c'],
                'col3': [1.1, 2.2, 3.3]}
        df = pd.DataFrame(data)
        wrong_object_columns = ['col1']
        wrong_int_columns = ['col2']

        with self.assertRaises(Exception):
            validate_types(df, wrong_object_columns, wrong_int_columns)
    def test_handle_missing_values(self):
        '''Test function returns dataframe with missing values handled'''
        data = {'col1': [1, None, 3],
                'col2': [None, 2, 4]}
        df = pd.DataFrame(data)

        result_df = handle_missing_values(df, [])

        expected_data = {'col1': [1.0, 2.0, 3.0],
                         'col2': [3.0, 2.0, 4.0]}
        expected_df = pd.DataFrame(expected_data)
        pd.testing.assert_frame_equal(result_df, expected_df)
    def test_handle_duplicate_values(self):
        data = {'col1': [1, 2, 3, 2, 4],
                'col2': ['a', 'b', 'c', 'b', 'd']}
        df = pd.DataFrame(data)

        result_df = handle_duplicate_values(df).reset_index()[['col1','col2']]
        expected_data = {'col1': [1, 2, 3, 4],
                         'col2': ['a', 'b', 'c', 'd']}
        expected_df = pd.DataFrame(expected_data).reset_index()[['col1','col2']]

        pd.testing.assert_frame_equal(result_df, expected_df)
    def test_calculate_ytd(self):
        data = {'price_date': ['2020-11-30', '2021-11-30'],
                'open': [10, 10],
                'close': [20, 20],
                }

        df = pd.DataFrame(data)

        result_df = calculate_ytd(df)

        expected_data = {'YTD': [1.00]}
        expected_df = pd.DataFrame(expected_data)

        pd.testing.assert_frame_equal(result_df, expected_df)
# Lots of room for more tests but didn't write due to time constraints. If I had some more time I'd write mock tests to test the s3 uploads etc
if __name__ == '__main__':
    unittest.main()