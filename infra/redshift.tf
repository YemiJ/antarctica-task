resource "aws_redshift_cluster" "redshift-cluster" {
  cluster_identifier        = "${local.prefix}-cluster"
  node_type                 = "dc2.large"
  database_name             = "task_database"
  master_username           = "db_user"
  master_password           = random_password.password.result
  cluster_type              = "single-node"
  cluster_subnet_group_name = aws_redshift_subnet_group.redshift-subnet-group.id
  skip_final_snapshot       = true
  iam_roles                 = [aws_iam_role.redshift_role.arn]
  depends_on = [
    aws_vpc.redshift-vpc,
    aws_default_security_group.redshift-security-group,
    aws_redshift_subnet_group.redshift-subnet-group,
    aws_iam_role.redshift_role,
    aws_s3_bucket.clean-s3-bucket
  ]
}

resource "random_password" "password" {
  length           = 16
  special          = true
  min_numeric      = 1
  override_special = "!#$%&*()-_=+[]{}<>:?"
}