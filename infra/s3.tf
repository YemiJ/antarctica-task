resource "aws_s3_bucket" "raw-s3-bucket" {
  bucket = "${local.prefix}-${var.raw-s3-bucket-name}"

  object_lock_enabled = false
  force_destroy       = true

}

resource "aws_s3_bucket_versioning" "raw-s3-bucket-versioning" {
  bucket = aws_s3_bucket.raw-s3-bucket.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "raw-s3-bucket-server-side-encryption-bucket" {
  bucket = aws_s3_bucket.raw-s3-bucket.id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket" "clean-s3-bucket" {
  bucket = "${local.prefix}-${var.clean-s3-bucket-name}"

  object_lock_enabled = false
  force_destroy       = true

  depends_on = [aws_s3_bucket.raw-s3-bucket]

}

resource "aws_s3_bucket_versioning" "clean-s3-bucket-versioning" {
  bucket = aws_s3_bucket.clean-s3-bucket.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "clean-s3-bucket-server-side-encryption-bucket" {
  bucket = aws_s3_bucket.clean-s3-bucket.id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket" "transformed-s3-bucket" {
  bucket = "${local.prefix}-${var.transformed-s3-bucket-name}"

  object_lock_enabled = false
  force_destroy       = true

  depends_on = [aws_s3_bucket.clean-s3-bucket]

}

resource "aws_s3_bucket_versioning" "transformed-s3-bucket-versioning" {
  bucket = aws_s3_bucket.transformed-s3-bucket.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "transformed-s3-bucket-server-side-encryption-bucket" {
  bucket = aws_s3_bucket.transformed-s3-bucket.id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}


################## Upload source data to s3 bucket  ##################
resource "aws_s3_object" "source-to-raw-s3" {
  for_each = fileset("./source-data/", "*")
  bucket   = aws_s3_bucket.raw-s3-bucket.id
  key      = each.key
  source   = "./source-data/${each.value}"
  etag     = filemd5("./source-data/${each.value}")
}