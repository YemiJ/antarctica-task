variable "aws_region" {
  default = "us-east-1"
}

variable "prefix" {
  default = "antarctica-task"
}

variable "project" {
  default = "antarctica-task-infra"
}

variable "contact" {
  default = "oluyemi.jegede@live.com"
}

variable "raw-s3-bucket-name" {
  default = "fund-data-raw"
}

variable "clean-s3-bucket-name" {
  default = "fund-data-clean"
}

variable "transformed-s3-bucket-name" {
  default = "fund-data-transformed"
}

variable "lambda2-function-name" {
  default = "lambda2_cleans3_to_transformeds3_and_redshift"
}

variable "lambda1-function-name" {
  default = "lambda1_raw_to_cleans3"
}

variable "fund_details_cleaned_file_name" {
  default = "fund_details_cleaned"
}

variable "fund_prices_cleaned_file_name" {
  default = "fund_prices_cleaned"
}

variable "db_port" {
  default = "5439"
}

variable "ytd_details_merged_file_name" {
  default = "ytd_fund_details_merged"
}