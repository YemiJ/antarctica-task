terraform {
  backend "s3" {
    bucket         = "tf-backend-antarctica-task-infra-tf-state"
    key            = "terraform.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "antarctica-task-infra-tf-state-lock"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.62.0"
    }

    docker = {
      source  = "kreuzwerker/docker"
      version = "3.0.2"
    }

  }
}

provider "aws" {
  region = "us-east-1"
}

locals {
  prefix = "${terraform.workspace}-${var.prefix}"
}

locals {
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}