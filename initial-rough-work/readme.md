### Initial rough work for antarctica task
Initial dive into task data to understand its intricacies.
#### Methods used
* Data visualisation 
* Data cleaning 
* Data analysis

#### Technologies used
* Python 
* Jupyter 
* Pandas 
* Seaborn (Matplotlib)

#### Getting started
* Clone this repo
* Run ```cd initial-rough-work/rough-work-files```
* Run ```make```
* Above command should build a Jupyter image and provide a link resembling http://127.0.0.1:8888/.... to access the jupyter server.

#### Featured images